package com.softdesign.mvpauth.di;

import android.util.Log;

import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DaggerService {
    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(DaggerService.class.getName());
    private static Map<Class, Object> sComponentMap = new HashMap<>();

    private static void registerComponent(Class componentClass, Object daggerComponent) {
        sComponentMap.put(componentClass, daggerComponent);
        Log.d(TAG, "registerClass: " + StringsUtil.getShortName(componentClass.getName()));
    }

    /*
    @Nullable
    @SuppressWarnings("uncheked")
    public static <T> T getComponent(Class<T> componentClass) {
        Object component = sComponentMap.get(componentClass);
        return (T) component;
    }
    */

    @SuppressWarnings("uncheked")
    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation) {
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnnotation)) {
                iterator.remove();
                Log.d(TAG, "registerClass: " + StringsUtil.getShortName(entry.getKey().getName()));
            }
        }
    }

    @SuppressWarnings("uncheked")
    public static <T> T createComponent(Class<T> componentClass, Class daggerClass, Object... dependencies) {
        Object component = sComponentMap.get(componentClass);
        if (component != null) {
            return (T) component;
        } else {
            try {
                Object builder = daggerClass.getMethod("builder").invoke(null);
                for (Method method : builder.getClass().getDeclaredMethods()) {
                    Class<?>[] params = method.getParameterTypes();
                    if (params.length == 1) {
                        Class<?> dependencyClass = params[0];
                        for (Object dependency : dependencies) {
                            if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                                method.invoke(builder, dependency);
                                break;
                            }
                        }
                    }
                }
                component = builder.getClass().getMethod("build").invoke(builder);
                if (component != null) {
                    registerComponent(componentClass, component);
                    return (T) component;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
