package com.softdesign.mvpauth.di.components;

import com.softdesign.mvpauth.di.modules.DataManagerModule;
import com.softdesign.mvpauth.mvp.models.AbstractModel;
import com.softdesign.mvpauth.ui.activities.RootActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {DataManagerModule.class})
@Singleton
public interface DataManagerInjector {
    void inject(RootActivity rootActivity);
    void inject(AbstractModel abstractModel);
}
