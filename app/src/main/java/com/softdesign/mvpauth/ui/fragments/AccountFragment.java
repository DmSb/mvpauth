package com.softdesign.mvpauth.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.UserDto;
import com.softdesign.mvpauth.databinding.FragmentAccountBinding;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AccountScope;
import com.softdesign.mvpauth.mvp.presenters.AccountPresenter;
import com.softdesign.mvpauth.mvp.views.IAccountView;
import com.softdesign.mvpauth.ui.adapters.AddressAdapter;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;
import com.softdesign.mvpauth.utils.TransformRoundedImage;

import javax.inject.Inject;

import dagger.Provides;

public class AccountFragment extends Fragment implements IAccountView {
    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(AccountFragment.class.getName());
    private FragmentAccountBinding mBinding;

    @Inject
    AccountPresenter mPresenter;

    // region ==================== LifeCycle ====================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);

        createDaggerComponent();

        mPresenter.takeView(this);
        mPresenter.initView();

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    // endregion

    // region ==================== IAccountView ====================

    @Override
    public void showAccountView(UserDto user) {
        Glide.with(getContext())
                .load(user.getAvatarUrl())
                .transform(new TransformRoundedImage(getContext()))
                .into(mBinding.accountImg);

        mBinding.setUser(user);
        mBinding.accountInfo.setUser(user);

        initAddressAdapter(user);
    }

    // endregion

    private void initAddressAdapter(UserDto user) {
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(mBinding.accountInfo.addressList.getContext());
        mBinding.accountInfo.addressList.setLayoutManager(linearLayoutManager);
        mBinding.accountInfo.addressList.setNestedScrollingEnabled(false);
        AddressAdapter adapter = new AddressAdapter(user.getAddresses(),
                new AddressAdapter.AddressViewHolder.CustomClickListener() {
                    @Override
                    public void onItemClickListener(View v, int position) {

                    }
                }
        );
        mBinding.accountInfo.addressList.swapAdapter(adapter, false);
    }

    // region ==================== IView ====================

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable e) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    // endregion

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerAccountFragment_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter () {
            return new AccountPresenter();
        }
    }

    @dagger.Component(modules = AccountFragment.Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountFragment fragment);
    }

    // endregion

}