package com.softdesign.mvpauth.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.AddressDto;
import com.softdesign.mvpauth.databinding.AddressItemBinding;

import java.util.List;

/**
 * Created by Дима on 09.11.2016.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> {

    private List<AddressDto> mAddresses;
    private AddressViewHolder.CustomClickListener mListener;

    public AddressAdapter(List<AddressDto> addresses, AddressViewHolder.CustomClickListener listener) {
        mAddresses = addresses;
        mListener = listener;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddressViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.address_item, parent, false),
                mListener
        );
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        holder.mBinding.setAddress(mAddresses.get(position));
    }

    @Override
    public int getItemCount() {
        return mAddresses.size();
    }

    public static class AddressViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        AddressItemBinding mBinding;
        CustomClickListener mClickListener;

        AddressViewHolder(View itemView, CustomClickListener clickListener) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
            mClickListener = clickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                int position = getAdapterPosition();
                mClickListener.onItemClickListener(view, position);
            }
        }

        public interface CustomClickListener {
            void onItemClickListener(View v, int position);
        }
    }
}
