package com.softdesign.mvpauth.ui.helpers;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

public class PasswordValidator implements TextWatcher {

    private final Context mContext;
    private final EditText mEdit;
    private final TextInputLayout mEditLayout;
    private final int mMessageResId;
    private boolean mIsValid = false;

    public PasswordValidator(Context context, EditText edit, TextInputLayout editLayout, int resId) {
        mContext = context;
        mEdit = edit;
        mEditLayout = editLayout;
        mMessageResId = resId;
    }

    public boolean isValid() {
        return mIsValid;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (mEdit != null) {
            mIsValid = validatePassword(editable.toString());
            if (mIsValid) {
                hideError();
            } else {
                showError();
            }
        }
    }

    private boolean validatePassword(String password) {
         return !TextUtils.isEmpty(password) &&
                 password.length() >= 8;
    }

    private void showError() {
        if (mEditLayout != null) {
            mEditLayout.setErrorEnabled(true);
            mEditLayout.setError(mContext.getString(mMessageResId));
        }
        mIsValid = false;
    }

    private void hideError() {
        if (mEditLayout != null) {
            mEditLayout.setErrorEnabled(false);
            mEditLayout.setError("");
        }
        mIsValid = true;
    }
}
