package com.softdesign.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.ui.helpers.EmailValidator;
import com.softdesign.mvpauth.ui.helpers.PasswordValidator;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

public class AuthPanel extends LinearLayout{

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(AuthPanel.class.getName());
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;
    private int mCustomState = IDLE_STATE;

    private CardView mAuthCard;
    private Button mLoginBtn, mShowCatalogBtn;
    private EditText mLoginEmailEt, mLoginPasswordEt;
    private EmailValidator mEmailValidator;
    private PasswordValidator mPasswordValidator;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        Log.d(TAG, "onFinishInflate");

        mLoginBtn = (Button) findViewById(R.id.login_btn);
        mShowCatalogBtn = (Button) findViewById(R.id.show_catalog_btn);
        mLoginEmailEt = (EditText) findViewById(R.id.login_email_et);
        mLoginPasswordEt = (EditText) findViewById(R.id.login_password_et);
        mAuthCard = (CardView) findViewById(R.id.auth_card);
        mEmailValidator = new EmailValidator(getContext(),
                mLoginEmailEt,
                (TextInputLayout) findViewById(R.id.login_email_wrap),
                R.string.email_error_value);
        mLoginEmailEt.addTextChangedListener(mEmailValidator);

        mPasswordValidator = new PasswordValidator(getContext(),
                mLoginPasswordEt,
                (TextInputLayout) findViewById(R.id.login_password_wrap),
                R.string.password_error_value);
        mLoginPasswordEt.addTextChangedListener(mPasswordValidator);

        showViewFromState(false);
    }



    @Override
    protected Parcelable onSaveInstanceState() {
        //Log.d(TAG, "onSaveInstanceState");

        Parcelable superState = super.onSaveInstanceState();
        SavedSate savedSate = new SavedSate(superState);
        savedSate.state = mCustomState;
        return savedSate;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        //Log.d(TAG, "onRestoreInstanceState");

        SavedSate savedSate = (SavedSate) state;
        super.onRestoreInstanceState(savedSate.getSuperState());
        setCustomState(savedSate.state);
    }

    public void setCustomState(int state) {
        //Log.d(TAG, "setCustomState " + String.valueOf(state));
        mCustomState = state;
        showViewFromState(true);
    }

    private void showLoginState() {
        //Log.d(TAG, "showLoginState");

        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        //Log.d(TAG, "showIdleState");
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }

    private void showViewFromState(boolean animateHide) {
        if (mCustomState == LOGIN_STATE) {
            //Log.d(TAG, "showViewFromState, mCustomState = LOGIN_STATE");
            showLoginState();
        } else {
            //Log.d(TAG, "showViewFromState, mCustomState = IDLE_STATE");
            if (animateHide) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.card_out);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        showIdleState();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                mAuthCard.startAnimation(animation);
            } else {
                showIdleState();
            }
        }
    }

    public String getUserEmail() {
        if (mEmailValidator.isValid()) {
            return String.valueOf(mLoginEmailEt.getText());
        } else {
            return "";
        }
    }

    public String getUserPassword() {
        if (mPasswordValidator.isValid()) {
            return String.valueOf(mLoginPasswordEt.getText());
        } else {
            return "";
        }
    }

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    static class SavedSate extends BaseSavedState {

        private int state;

        public static final Parcelable.Creator<SavedSate> CREATOR = new Parcelable.Creator<SavedSate>() {

            @Override
            public SavedSate createFromParcel(Parcel parcel) {
                 return new SavedSate(parcel);
            }

            @Override
            public SavedSate[] newArray(int size) {
                return new SavedSate[size];
            }
        };

        SavedSate(Parcelable superState) {
            super(superState);
        }

        SavedSate(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }
}
