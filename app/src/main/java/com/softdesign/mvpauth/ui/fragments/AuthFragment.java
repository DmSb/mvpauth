package com.softdesign.mvpauth.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.databinding.FragmentAuthBinding;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.presenters.AuthPresenter;
import com.softdesign.mvpauth.mvp.presenters.IAuthPresenter;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

import javax.inject.Inject;

import dagger.Provides;

public class AuthFragment extends Fragment
        implements IAuthView, View.OnClickListener {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(AuthFragment.class.getName());
    private FragmentAuthBinding mBinding;

    @Inject
    AuthPresenter mPresenter;

    // region ==================== Lifecycle ====================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);

        createDaggerComponent();

        mPresenter.takeView(this);
        mPresenter.initView();

        mBinding.loginBtn.setOnClickListener(this);
        mBinding.showCatalogBtn.setOnClickListener(this);

        readBundle(getArguments());
        if (mPresenter.getCallRootActivity()) {
            mBinding.showCatalogBtn.setText(getString(R.string.return_to_catalog));
        }

        return mBinding.getRoot();
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mPresenter.setCallRootActivity(bundle.getBoolean("CALL_ROOT_ACTIVITY", false));
        }
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mBinding.loginBtn.setOnClickListener(null);
        mBinding.showCatalogBtn.setOnClickListener(null);
        mPresenter.dropView();
        super.onDestroyView();
    }

    // endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.social_vk_btn:
                mPresenter.clickOnVk();
                break;
            case R.id.social_twitter_btn:
                mPresenter.clickOnTwitter();
                break;
            case R.id.social_fb_btn:
                mPresenter.clickOnFb();
                break;
        }
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    // region ==================== IAuthView ====================

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mBinding.loginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mBinding.loginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mBinding.authWrapper;
    }

    @Override
    public void showCatalogScreen() {
        getRootActivity().showCatalogScreen();
    }

    // endregion

    // region ==================== IView ====================

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().showLoad();
    }

    // endregion

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerAuthFragment_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthPresenter provideAuthPresenter () {
            return new AuthPresenter();
        }
    }

    @dagger.Component(modules = AuthFragment.Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthFragment authFragment);
    }

    // endregion
}