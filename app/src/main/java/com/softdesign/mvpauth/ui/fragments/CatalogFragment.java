package com.softdesign.mvpauth.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.databinding.FragmentCatalogBinding;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.CatalogScope;
import com.softdesign.mvpauth.mvp.presenters.CatalogPresenter;
import com.softdesign.mvpauth.mvp.views.ICatalogView;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.adapters.CatalogAdapter;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    private FragmentCatalogBinding mBinding;

    @Inject
    CatalogPresenter mPresenter;

    public CatalogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_catalog, container, false);

        createDaggerComponent();

        mPresenter.takeView(this);
        mPresenter.initView();

        mBinding.addToCardBtn.setOnClickListener(this);

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mBinding.addToCardBtn.setOnClickListener(null);
        mPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_to_card_btn:
                mPresenter.clickOnByBtn(mBinding.productPager.getCurrentItem());
                break;
        }
    }

    // region =============================== ICatalogView =========================================

    @Override
    public void showAddToCartMessage(ProductDto product) {
        showMessage("Товар " + product.getProductName() + "успешно добавлен в корзину");
    }

    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productList) {
            adapter.addItem(product);
        }
        mBinding.productPager.setAdapter(adapter);
        mBinding.pagerIndicator.setViewPager(mBinding.productPager);
    }

    @Override
    public void showAuthScreen() {
        getRootActivity().showAuthScreen();
    }

    @Override
    public void updateRootProductCounter(int productCount) {
        getRootActivity().updateProductCounter(productCount);
    }

    // endregion

    // region ================================== IView =============================================

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    // endregion

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerCatalogFragment_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter () {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = CatalogFragment.Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogFragment catalogFragment);
    }

    // endregion
}
