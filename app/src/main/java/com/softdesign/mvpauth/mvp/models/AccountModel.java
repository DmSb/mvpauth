package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.storage.dto.UserDto;

public class AccountModel extends AbstractModel {

    public AccountModel() {
    }

    public UserDto getUser(){
        return mDataManager.getUser();
    }
}
