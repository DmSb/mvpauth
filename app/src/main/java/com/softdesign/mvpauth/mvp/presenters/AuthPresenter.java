package com.softdesign.mvpauth.mvp.presenters;

import android.os.Handler;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.models.AuthModel;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

import javax.inject.Inject;

import dagger.Provides;

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {

    @Inject
    AuthModel mModel;

    public AuthPresenter() {
        createDaggerComponent();
    }

    // region ==================== IAuthPresenter ====================

    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                String userEmail = getView().getAuthPanel().getUserEmail();
                String userPassword = getView().getAuthPanel().getUserPassword();

                if (!userEmail.isEmpty() && !userPassword.isEmpty()) {
                    getView().showLoad();
                    runWithDelay();
                    mModel.loginUser(userEmail, userPassword);
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            getView().showCatalogScreen();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }

    @Override
    public void setCallRootActivity(boolean callRootActivity) {

    }

    @Override
    public boolean getCallRootActivity() {
        return false;
    }

    // endregion

    private void runWithDelay(){
        final Handler handler = new Handler();
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (getView() != null) {
                            getView().hideLoad();
                            if (mModel.isUserAuth()) {
                                getView().showCatalogScreen();
                            } else {
                                getView().showMessage("Ошибка авторизации");
                            }
                        }
                    }
                }, 3000
        );
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerAuthPresenter_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthModel provideAuthModel () {
            return new AuthModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthPresenter presenter);
    }


    // endregion
}
