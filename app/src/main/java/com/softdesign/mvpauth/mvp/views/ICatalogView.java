package com.softdesign.mvpauth.mvp.views;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;

import java.util.List;

public interface ICatalogView extends IView {

    void showAddToCartMessage(ProductDto product);
    void showCatalogView(List<ProductDto> productList);
    void showAuthScreen();
    void updateRootProductCounter(int productCount);
}
