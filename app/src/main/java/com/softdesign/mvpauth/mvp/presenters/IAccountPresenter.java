package com.softdesign.mvpauth.mvp.presenters;

interface IAccountPresenter {

    void initView();
    void onPhoneChanged();
    void onPushStatusChanged();
    void onPushActionChanged();
    void onAddAddress();
}
