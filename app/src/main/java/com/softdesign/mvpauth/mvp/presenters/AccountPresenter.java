package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AccountScope;
import com.softdesign.mvpauth.mvp.models.AccountModel;
import com.softdesign.mvpauth.mvp.views.IAccountView;

import javax.inject.Inject;

import dagger.Provides;

public class AccountPresenter extends AbstractPresenter<IAccountView> implements IAccountPresenter {

    @Inject
    AccountModel mModel;

    public AccountPresenter() {
        createDaggerComponent();
    }

    // region ==================== IAccountPresenter ====================

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showAccountView(mModel.getUser());
        }
    }

    @Override
    public void onPhoneChanged() {

    }

    @Override
    public void onPushStatusChanged() {

    }

    @Override
    public void onPushActionChanged() {

    }

    @Override
    public void onAddAddress() {

    }

    // endregion

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerAccountPresenter_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountModel provideAccountModel () {
            return new AccountModel();
        }
    }

    @dagger.Component(modules = AccountPresenter.Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);
    }

    // endregion

}
