package com.softdesign.mvpauth.mvp.views;

import com.softdesign.mvpauth.data.storage.dto.UserDto;

public interface IAccountView extends IView {
    void showAccountView(UserDto user);
}
