package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.ProductScope;
import com.softdesign.mvpauth.mvp.models.ProductModel;
import com.softdesign.mvpauth.mvp.views.IProductView;
import com.softdesign.mvpauth.utils.ConstantManager;

import javax.inject.Inject;

import dagger.Provides;

public class ProductPresenter extends AbstractPresenter<IProductView>
        implements IProductPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX + ProductPresenter.class.getCanonicalName();

    @Inject
    ProductModel mModel;
    private ProductDto mProduct;

    public ProductPresenter(ProductDto product) {
        mProduct = product;
        createDaggerComponent();
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
        }
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerProductPresenter_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {

        @Provides
        @ProductScope
        ProductModel provideProductModel () {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = ProductPresenter.Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);
    }

    // endregion
}
