package com.softdesign.mvpauth.mvp.models;

public class AuthModel extends AbstractModel{

    public AuthModel() {
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }

    public void loginUser(String email, String password) {
        if (!email.isEmpty() && !password.isEmpty()) {
            mDataManager.saveToken(email + " " + password);
        }
    }
}