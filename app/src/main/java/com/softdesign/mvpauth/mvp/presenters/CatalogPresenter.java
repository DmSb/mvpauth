package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.CatalogScope;
import com.softdesign.mvpauth.mvp.models.CatalogModel;
import com.softdesign.mvpauth.mvp.views.ICatalogView;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class CatalogPresenter extends AbstractPresenter<ICatalogView>
        implements ICatalogPresenter {

    @Inject
    CatalogModel mModel;

    private List<ProductDto> mProductList;
    private int mRootProductCount = 0;

    public CatalogPresenter() {
        createDaggerComponent();
    }

    @Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }
    }

    @Override
    public void clickOnByBtn(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                addProductToCard(position);
                getView().updateRootProductCounter(mRootProductCount);
                getView().showAddToCartMessage(mProductList.get(position));
            } else {
                getView().showAuthScreen();
            }
        }

    }

    @Override
    public boolean checkUserAuth() {
        return mModel.checkUserAuth();
    }

    @Override
    public void addProductToCard(int position) {
        mRootProductCount += mProductList.get(position).getCount();
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerCatalogPresenter_Component.class,
                new Module());
        if (component != null)
            component.inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel () {
            return new CatalogModel();
        }
    }

    @dagger.Component(modules = CatalogPresenter.Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter presenter);
    }

    // endregion
}
