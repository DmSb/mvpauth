package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.storage.dto.ProductDto;

public class ProductModel extends AbstractModel{

    public ProductDto getProductById(int productId) {
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
