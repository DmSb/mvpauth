package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.managers.DataManager;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.components.DaggerDataManagerInjector;
import com.softdesign.mvpauth.di.components.DataManagerInjector;
import com.softdesign.mvpauth.di.modules.DataManagerModule;

import javax.inject.Inject;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    AbstractModel() {
        createDaggerComponent();
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        DataManagerInjector component = DaggerService.createComponent(DataManagerInjector.class,
                DaggerDataManagerInjector.class,
                new DataManagerModule());
        if (component != null)
            component.inject(this);
    }

    // endregion
}