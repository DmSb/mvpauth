package com.softdesign.mvpauth.utils;

public class StringsUtil {
    public static String getShortName(String className) {
        return className.substring(className.lastIndexOf(".") + 1);
    }
}
