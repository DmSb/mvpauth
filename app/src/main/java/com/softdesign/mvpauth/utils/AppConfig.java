package com.softdesign.mvpauth.utils;

public interface AppConfig {
    String BASE_URL = "http://devintensive.softdesign-apps.ru/api/";
    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;
}
