package com.softdesign.mvpauth;

import android.app.Application;
import android.content.Context;

import com.softdesign.mvpauth.utils.FontUtil;

import dagger.Provides;

public class App extends Application {

    private static Component sComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        FontUtil.initFont(this);
        createDaggerComponent();
    }

    public static Component getComponent() {
        return sComponent;
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        sComponent = DaggerApp_Component.builder()
                .module(new Module(getApplicationContext()))
                .build();
    }

    @dagger.Module
    public class Module {

        private Context mContext;

        public Module(Context context) {
            mContext = context;
        }

        @Provides
        Context provideContext () {
            return mContext;
        }
    }

    @dagger.Component(modules = Module.class)
    public interface Component {
        Context getContext();
    }

    // endregion
}