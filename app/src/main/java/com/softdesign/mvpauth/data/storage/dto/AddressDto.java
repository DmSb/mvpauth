package com.softdesign.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class AddressDto implements Parcelable {
    private int id;
    private String place;
    private String street;
    private String home;
    private String flate;
    private String office;
    private String comment;

    public AddressDto(int id, String place, String street, String home, String flate, String office, String comment) {
        this.id = id;
        this.place = place;
        this.street = street;
        this.home = home;
        this.flate = flate;
        this.office = office;
        this.comment = comment;
    }

    // region ==================== Getters ====================

    public int getId() {
        return id;
    }

    public String getPlace() {
        return place;
    }

    public String getStreet() {
        return street;
    }

    public String getHome() {
        return home;
    }

    public String getFlate() {
        return flate;
    }

    public String getOffice() {
        return office;
    }

    public String getComment() {
        return comment;
    }

    public String getFullAdsress() {
        String s = street;
        if (!home.isEmpty())
            s += ", " + home;
        if (!flate.isEmpty())
            s += " - " + flate;
        if (!office.isEmpty())
            s += ", " + office;
        return s;
    }

    // endregion

    // region ==================== Parcelable ====================

    public AddressDto(Parcel in) {
        this.id = in.readInt();
        this.place = in.readString();
        this.street = in.readString();
        this.home = in.readString();
        this.flate = in.readString();
        this.office = in.readString();
        this.comment = in.readString();
    }

    public static final Creator<AddressDto> CREATOR = new Creator<AddressDto>() {
        @Override
        public AddressDto createFromParcel(Parcel in) {
            return new AddressDto(in);
        }

        @Override
        public AddressDto[] newArray(int size) {
            return new AddressDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(place);
        parcel.writeString(street);
        parcel.writeString(home);
        parcel.writeString(flate);
        parcel.writeString(office);
        parcel.writeString(comment);
    }

    // endregion
}
