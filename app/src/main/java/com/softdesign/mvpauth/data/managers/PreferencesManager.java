package com.softdesign.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.softdesign.mvpauth.utils.ConstantManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferencesManager {

    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.TOKEN_KEY, token);
        editor.apply();
    }

    String getToken() {
        return mSharedPreferences.getString(ConstantManager.TOKEN_KEY, "");
    }

    void saveUserName(String userName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_NAME_KEY, userName);
        editor.apply();
    }

    String getUserName() {
        return mSharedPreferences.getString(ConstantManager.USER_NAME_KEY, "");
    }

    public String getUserPhone() {
        return mSharedPreferences.getString(ConstantManager.USER_PHONE_KEY, "");
    }

    public void setUserPhone(String userPhone) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_PHONE_KEY, userPhone);
        editor.apply();
    }

    public List<String> getAddresses() {
        List<String> list = new ArrayList<>();
        Set<String> adreses = mSharedPreferences.getStringSet(ConstantManager.USER_ADDRESS_KEY, new HashSet<String>());
        for (String adres: adreses) {
            list.add(adres);
        }
        return list;
    }
}
