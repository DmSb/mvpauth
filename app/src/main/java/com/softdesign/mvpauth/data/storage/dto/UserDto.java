package com.softdesign.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class UserDto implements Parcelable {

    private String name;
    private String avatarUrl;
    private String email;
    private String phone;
    private List<AddressDto> addresses;
    private boolean pushState;
    private boolean pushAction;

    public UserDto(String name, String avatarUrl, String email, String phone,
                   List<AddressDto> addresses, boolean pushState, boolean pushAction) {
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.email = email;
        this.phone = phone;
        this.addresses = addresses;
        this.pushState = pushState;
        this.pushAction = pushAction;
    }

    // region ==================== Getters ====================

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public List<AddressDto> getAddresses() {
        return addresses;
    }

    public boolean isPushState() {
        return pushState;
    }

    public boolean isPushAction() {
        return pushAction;
    }

    // endregion

    // region ==================== Parcelable ====================

    protected UserDto(Parcel in) {
        this.name = in.readString();
        this.avatarUrl = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.addresses = in.readArrayList(AddressDto.class.getClassLoader());
        if (in.readInt() == 0) {
            this.pushState = false;
        } else {
            this.pushState = true;
        }
        if (in.readInt() == 0) {
            this.pushAction = false;
        } else {
            this.pushAction = true;
        }
    }

    public static final Creator<UserDto> CREATOR = new Creator<UserDto>() {
        @Override
        public UserDto createFromParcel(Parcel in) {
            return new UserDto(in);
        }

        @Override
        public UserDto[] newArray(int size) {
            return new UserDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(avatarUrl);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeList(addresses);
        if (pushState) {
            parcel.writeInt(1);
        } else {
            parcel.writeInt(0);
        }
        if (pushAction) {
            parcel.writeInt(1);
        } else {
            parcel.writeInt(0);
        }
    }

    // endregion
}
