package com.softdesign.mvpauth.data.managers;

import com.softdesign.mvpauth.App;
import com.softdesign.mvpauth.data.network.RestService;
import com.softdesign.mvpauth.data.storage.dto.AddressDto;
import com.softdesign.mvpauth.data.storage.dto.ProductDto;
import com.softdesign.mvpauth.data.storage.dto.UserDto;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.modules.LocalModule;
import com.softdesign.mvpauth.di.modules.NetworkModule;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.StringsUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

public class DataManager {
    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringsUtil.getShortName(DataManager.class.getName());

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;
    private UserDto mUser;

    public DataManager() {
        //Log.d(TAG, "DataManager create");

        createDaggerComponent();
        generateMockData();

        tmpCreateUser();
    }

    private void tmpCreateUser() {
        List<AddressDto> addresses = new ArrayList<>();
        addresses.add(new AddressDto(1, "Дом", "Строителей", "5", "12", "6 офис", "с лифта направо"));
        addresses.add(new AddressDto(2, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(3, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(4, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(5, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(6, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(7, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(8, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        addresses.add(new AddressDto(9, "Дом", "Строителей", "5", "12", "8 офис", "с лифта налево"));
        mUser = new UserDto("Anastasy", "file:///android_asset/16w.png",
                "email@mail.ru", "+7 (123) 456-78-90", addresses, false, true);
    }

    // region ==================== Dagger ====================

    @dagger.Component(dependencies = App.Component.class,
            modules =  {LocalModule.class, NetworkModule.class})
    @Singleton
    public interface Component {
        void inject(DataManager dataManager);
    }

    public void createDaggerComponent() {
        Component component = DaggerService.createComponent(Component.class,
                DaggerDataManager_Component.class,
                App.getComponent(),
                new LocalModule(),
                new NetworkModule());
        if (component != null)
            component.inject(this);
    }

    // endregion

    // region ==================== SharedPreferences ====================

    public void saveToken(String token) {
        mPreferencesManager.saveToken(token);
    }

    public boolean isAuthUser() {
        return ! mPreferencesManager.getToken().isEmpty();
    }

    public UserDto getUser() {
        return mUser;
    }

    // endregion

    // region ==================== TempDataFactory ====================

    public ProductDto getProductById(int productId) {
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto product) {

    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Riga-120B", "https://pp.vk.me/c631227/v631227752/20b53/xJlf5YYr39Q.jpg", "Кассетная магнитола", 100, 1));
        mMockProductList.add(new ProductDto(2, "Маяк-205", "https://pp.vk.me/c631227/v631227752/20b41/_3MJfl60IiM.jpg", "Катушечный магнитофон", 200, 1));
        mMockProductList.add(new ProductDto(3, "Иж-303", "https://pp.vk.me/c627525/v627525737/4013b/BRJL29dHfEQ.jpg", "Кассетный магнитофон", 300, 1));
        mMockProductList.add(new ProductDto(4, "Океан-209", "https://pp.vk.me/c627525/v627525737/40129/6LcyXS86dpE.jpg", "Радиоприемник", 400, 1));
        mMockProductList.add(new ProductDto(5, "Маяк-001 Стерео", "https://pp.vk.me/c633630/v633630486/18775/aiZwadWQx64.jpg", "Катушечный магнитофон высшего класса", 500, 1));
        mMockProductList.add(new ProductDto(6, "Вега МП-122С", "https://pp.vk.me/c628417/v628417283/1d919/I4pPzyKTctQ.jpg", "Стерефонический двухкассетный магнитофон\nВнешний усилитель Вега 50У-122С", 600, 1));
        mMockProductList.add(new ProductDto(7, "Комета-225", "https://pp.vk.me/c410427/v410427205/5589/yWEbdr-55Ak.jpg", "Однокассетный стерефонический магнитофон с акустическими системами", 700, 1));
    }

    public List<ProductDto> getProductList() {
        return mMockProductList;
    }

    // endregion
}
